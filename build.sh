#!/usr/bin/env bash
# will create dist/x.y.z/ folder where x.y.z from package.json version
REVISION=$(node -e 'console.log(require("./package.json").version)')
DIST_DIR=dist/$REVISION/
BUNDLE=$DIST_DIR/xml4jquery.bundle.js
DEST=$DIST_DIR/xml4jquery.bin.js
JS=$DIST_DIR/xml4jquery.js

rm -rf $DIST_DIR
if [ ! -d "dist" ]; then
    mkdir dist
fi

mkdir $DIST_DIR
mkdir $DIST_DIR/test

cp index.html $DIST_DIR
cp package.json $DIST_DIR
cp -R test $DIST_DIR
mkdir $DIST_DIR/node_modules
cp -R -u node_modules/jquery $DIST_DIR/node_modules
cp -R -u node_modules/intern $DIST_DIR/node_modules
cp -R -u node_modules/babel-polyfill $DIST_DIR/node_modules

echo ";(function(){ 'use strict'; var define,XmlAspect;" >$BUNDLE
cat lib/AMD/xml/xml.js      >>$BUNDLE
cat lib/AMD/xml/xml_ie.js   >>$BUNDLE
cat xml4jquery.js       >>$BUNDLE
echo "})();" >>$BUNDLE

java -jar node_modules/google-closure-compiler/compiler.jar --compilation_level SIMPLE --js_output_file=$DEST $BUNDLE
# java -jar node_modules/google-closure-compiler/compiler.jar --compilation_level WHITESPACE_ONLY --js_output_file=$DEST $BUNDLE
# java -jar node_modules/google-closure-compiler/compiler.jar --compilation_level ADVANCED_OPTIMIZATIONS --js_output_file=$DEST $BUNDLE
rm $BUNDLE


echo "/*">$JS
cat COPYRIGHT-binary >>$JS
echo "xml4jQuery rev $REVISION">>$JS
echo "*/">>$JS
cat $DEST>>$JS
rm $DEST
echo $DIST_DIR
