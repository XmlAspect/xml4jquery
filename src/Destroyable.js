/**
 * Created by sashafir on 3/6/2016.
 */
define( function()
{   "use strict";
    class Destroyable
    {
        constructor() // todo check whether cb is useful as parameter
        {   var childList   = []
            ,   ownersList  = []
            ,   isDestroyed;
            this.addDestroyableChild = c => childList.push(c);

            this.destroy = function()// releases own and owned children resources.
            {
                if( isDestroyed )
                    return;
                isDestroyed = 1;
                //     1.  unlock/release/remove own resources. No async functionality shall be kept in queue.
                //     2.  invoke children[].destroy()
                //     3.  reject own promise with InterruptedException
                //     4.  detach from parent
                //     5.  invoke parent.destroy()
                var ch = childList;
                childList = [];
                detach.call(this);
                ch.forEach( c => c.destroy() );
            };
            function detach( /*Destroyable | undefined */ child )
            {
                if( child )
                    return childList.splice( childList.indexOf(child),1 );
                //todo detach from parents
                this.onDetachDestroyable(this);
            };
        }
        onDetachDestroyable(el){}// to be overridden
    }
    return Destroyable;
} );