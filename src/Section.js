/**
 * Created by sashafir on 2/5/2016.
 */
define( ["./Destroyable"],function( Destroyable )
{   "use strict";
    class Section extends Destroyable
    {
        /***
         *  Presentation of scenario method call.
         *  Comprise of
         *      * method name
         *      * declare time arguments list
         *      * call time serving Object reference
         *      * ?call time arguments
         *      * prototype scope list of async methods wrappers which creating the next Section in scenario chain.
         *
         *
         ***/
        constructor( obj, methodName )
        {
            super();
            this.args = arguments;
        }
        onDetachDestroyable( el )
        {


        }
        fork()
        {   /** create new current segment tree clone with own promise chain starting from current segment.
                @return clone of current Segment
            **/

        }
    }
    var $ = Section;
    $.InitializePrototype = function(o)
    {
        for( var k in o )
        {   if( !$[k] )
            {   $[k] = [];
                $.prototype[k] = getWrapper(k);
            }
            $[k].push( o[k] );
        }
    };
    return Section;
    function getWrapper(k)
    {
        return function()
        {
            this[k].apply(arguments);
        }
    }
} );