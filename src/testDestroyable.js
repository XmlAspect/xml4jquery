/*********************************************************************
 *
 * SIMULATION WORKS CONFIDENTIAL
 * _____________________________
 *
 *  Copyright 2016 Simulation Works, LLC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Simulation Works, LLC.  The intellectual and
 * technical concepts contained herein are proprietary to
 * Simulation Works, LLC and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Simulation Works, LLC.
 *********************************************************************/
define( ["require", 'intern!object', 'test/assert'
    , '../Destroyable'
], function( require, registerSuite, assert, Destroyable )
{   "use strict";
    // http://localhost/xml4jquery/node_modules/intern/client.html?config=src/test/intern&initialBaseUrl=/xml4jquery&suites=src/test/testDestroyable
    registerSuite(
    {   "single Destroyable, constructor,  destroy, onDetachDestroyable"     : function()
        {   assert( 'function' == typeof Destroyable.prototype.constructor );
            var a = new Destroyable();
            a.onDetachDestroyable = function(){ this.destroyed =1; }
            assert( !a.destroyed );
            a.destroy();
            assert( a.destroyed );
        }
    ,   "single child":
        {   "destroy child":function()
            {   var a = new Destroyable()
                ,   b = new Destroyable();
                a.onDetachDestroyable = b.onDetachDestroyable = function(){ this.destroyed =1; }
                a.addDestroyableChild( b )
                assert( !a.destroyed && !b.destroyed );
                b.destroy();
                assert( !a.destroyed && b.destroyed );
                b.destroyed = 2;
                a.destroy();
                assert( a.destroyed && 2 == b.destroyed, "b.onDetachDestroyable() should not be called as it has been destroyed" );
            }
        ,   "destroy parent":function()
            {   var a = new Destroyable()
                ,   b = new Destroyable();
                a.onDetachDestroyable = b.onDetachDestroyable = el => el.destroyed=1;
                a.addDestroyableChild( b )
                assert( !a.destroyed && !b.destroyed );
                a.destroy();
                assert( a.destroyed && b.destroyed );
                a.destroyed = b.destroyed = 2;
                a.destroy();
                assert( 2 == a.destroyed && 2 == b.destroyed, "onDetachDestroyable() should not be called as it has been destroyed" );
            }
        }
    ,   "3 leyers, middle destroyed": function()
        {   var a = create()
            ,   b1 = create()
            ,   b2 = create()
            ,   b3 = create()
            ,   c1 = create()
            ,   c2 = create()
            ,   c3 = create();
            a.addDestroyableChild(b1);
            a.addDestroyableChild(b2);
            a.addDestroyableChild(b3);
            b2.addDestroyableChild(c1);
            b2.addDestroyableChild(c2);
            b2.addDestroyableChild(c3);
            assert( !a.destroyed );
            assert( !b1.destroyed && !b2.destroyed && !b3.destroyed );
            assert( !c1.destroyed && !c2.destroyed && !c3.destroyed );
            c2.destroy();
            assert( !a.destroyed );
            assert( !b1.destroyed && !b2.destroyed && !b3.destroyed );
            assert( !c1.destroyed &&  c2.destroyed && !c3.destroyed );
            c2.destroyed = 2; // should not be changed since destroyed
            b2.destroy();
            assert( !a.destroyed );
            assert( !b1.destroyed &&  b2.destroyed && !b3.destroyed );
            assert(  c1.destroyed &&  2 == c2.destroyed &&  c3.destroyed );            
        }
    ,   "inherited onDetachDestroyable": function()
        {   var a = new DestroyableSample()
            ,   b = new DestroyableSample();
            a.addDestroyableChild( b )
            assert( 0===a.detached && 0===b.detached );
            a.destroy();
            assert( a.detached && b.detached );
            a.detached = b.detached = 2;
            a.destroy();
            assert( 2 == a.detached && 2 == b.detached, "onDetachDestroyable() should not be called twice" );
        }
    });

    function create()
    {
        var a = new Destroyable();
        a.onDetachDestroyable = el => el.destroyed=1;
        return a                        
    }
    class DestroyableSample extends Destroyable
    {   constructor(){super(); this.detached = 0; }
        onDetachDestroyable(){ this.detached = 1; }
    }
    function createTree()
    {
        var a = new Destroyable()
        ,   b = new Destroyable();
        a.onDetachDestroyable = ()=> this.refs--;
    }
    function NOP(){}
    function RETHROW(ex){throw ex;}
    function PASS(d){ return function()
    {
        d.resolve(1);
    } }
    function ERR (d){ return function(err)
    {   debugger;
        d.reject(err);
    } }
    function assertD( d, cond )
    {   if( cond )
            return;
        debugger;
        d.reject();
    }
});