/*********************************************************************
 *
 * SIMULATION WORKS CONFIDENTIAL
 * _____________________________
 *
 *  Copyright 2016 Simulation Works, LLC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Simulation Works, LLC.  The intellectual and
 * technical concepts contained herein are proprietary to
 * Simulation Works, LLC and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Simulation Works, LLC.
 *********************************************************************/
define( ["require", 'intern!object', 'test/assert'
    , '../Section'
], function( require, registerSuite, assert, Section )
{   "use strict";
    // http://localhost/xml4jquery/node_modules/intern/client.html?config=src/test/intern&initialBaseUrl=/xml4jquery&suites=src/test/testPlugins
    registerSuite(
    {
        // 1. registration
        // 2. chaining
        // 3. implementation call
        // 4. delayed call
        // 5. promise() and promise methods
        // 6. object wrapper
        // 7. method marshalling
        // 8. collection wrapping, element marshalling

        "single Destroyable, constructor,  destroy, onDetachDestroyable"     : function()
        {   assert( 'function' == typeof $.fn.sleep );
            assert( 'function' == typeof $().sleep  );


            $.define('scenario name',['sleep','attr'])
                .sleep().attr();// ...

            // default scenario (blank name) lists x4j plugins and all jq methods .
            $().$on();  // requires $on plugin along with Promise creation.
                        // Upon promise and require completion resolves own Promise with result of plugin call

            var a = new Destroyable();
            a.onDetachDestroyable = function(){ this.destroyed =1; }
            assert( !a.destroyed );
            a.destroy();
            assert( a.destroyed );
        }
    ,   "inherited Destroyable": function()
        {   var a = new Section()
            ,   b = new Section();
            a.addDestroyableChild( b )
            assert( 0===a.detached && 0===b.detached );
            a.destroy();
            assert( a.detached && b.detached );
            a.detached = b.detached = 2;
            a.destroy();
            assert( 2 == a.detached && 2 == b.detached, "onDetachDestroyable() should not be called twice" );
        }
    });

    function create()
    {
        var a = new Destroyable();
        a.onDetachDestroyable = el => el.destroyed=1;
        return a                        
    }
    class DestroyableSample extends Destroyable
    {   constructor(){super(); this.detached = 0; }
        onDetachDestroyable(){ this.detached = 1; }
    }
    function createTree()
    {
        var a = new Destroyable()
        ,   b = new Destroyable();
        a.onDetachDestroyable = ()=> this.refs--;
    }
    function NOP(){}
    function RETHROW(ex){throw ex;}
    function PASS(d){ return function()
    {
        d.resolve(1);
    } }
    function ERR (d){ return function(err)
    {   debugger;
        d.reject(err);
    } }
    function assertD( d, cond )
    {   if( cond )
            return;
        debugger;
        d.reject();
    }
});