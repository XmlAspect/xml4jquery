/*********************************************************************
 *
 * SIMULATION WORKS CONFIDENTIAL
 * _____________________________
 *
 *  Copyright 2016 Simulation Works, LLC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Simulation Works, LLC.  The intellectual and
 * technical concepts contained herein are proprietary to
 * Simulation Works, LLC and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Simulation Works, LLC.
 *********************************************************************/
define( ["require", 'intern!object', 'test/assert'
    , '../Section'
], function( require, registerSuite, assert, Section )
{   "use strict";
    // http://localhost/xml4jquery/node_modules/intern/client.html?config=src/test/intern&initialBaseUrl=/xml4jquery&suites=src/test/testDestroyable
    registerSuite(
    {   "Section.InitializePrototype"     : function()
        {

            Section.InitializePrototype({   test1:f1 });
            var s1 = new Section('test1');

            assert( 'function' == typeof Section.prototype.test1 );
            assert( 'function' == typeof s1.test1 );

            Section.InitializePrototype({   test2:f2, test1:f2  });
            var s2 = new Section('test1');

            assert( 'function' == typeof Section.prototype.test2 );
            assert( 'function' == typeof s1.test2 );

            function f1(){}
            function f2(){}
        }
    ,   "wrapped object method invocation": function()
        {   var o = {test1:function(){this.otest=1;}}
            ,   a = new Section(o);
            a.test1();
            assert( undefined===o.otest1, "object method should be called only upon fork()" );
            assert( undefined===o.test1 );
        }
    ,   "call chaining": function()
        {   var o = {}
            ,   a = new Section(o)
            ,   b = a.test1();
            assert( b instanceof Section );
            assert( 'function' == typeof b.test2 );
        }
    ,   "fork": function()
        {   var o = {test1:function(){this.otest=1;}}
            ,   a = new Section(o)
            ,   b = a.test1();
            a.fork();
            assert( o.otest );
        }
    });
    function NOP(){}
    function RETHROW(ex){throw ex;}
    function PASS(d){ return function()
    {
        d.resolve(1);
    } }
    function ERR (d){ return function(err)
    {   debugger;
        d.reject(err);
    } }
    function assertD( d, cond )
    {   if( cond )
            return;
        debugger;
        d.reject();
    }
});