/*********************************************************************
 *
 * SIMULATION WORKS CONFIDENTIAL
 * _____________________________
 *
 *  Copyright 2015 Simulation Works, LLC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Simulation Works, LLC.  The intellectual and
 * technical concepts contained herein are proprietary to
 * Simulation Works, LLC and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Simulation Works, LLC.
 *********************************************************************/
(function (root, factory)
{	if( typeof define === 'function' && define.amd )
    define(["xml"], factory);	// AMD
else factory( XmlAspect );	// browser
}( this, function( XmlAspect )
{	"use strict";
    var $ = jQuery
    ,   extFn = 
        {   xPath       : XPath
        ,   xmlTransform: xmlTransform
        ,   loadXml     : loadXml
        ,   sleep       : function(ms){   debugger; return ms || 0; }
        ,   $then       : function(onOK, onErr){   try{ return onOK && onOK(); }catch(ex){ return onErr(ex);} }
        };
    $.extend( $.fn, extFn );

    function XPath( xpath, el )
    {   try
        {   return XPath_inline.apply(this, arguments);
        }catch(ex)
                { console.error(ex, xpath, el); }
        return $();
    }
    function XPath_async( xpath, el )
    {   var args = arguments;
        return this.$then(function($xml)
        {
            return XPath_inline.apply( $xml, args );
        });
    }
    function XPath_inline( xpath, el )
    {
        var $ret = $([]);

        if(!el )
            el = this;
        $( el ).each( function( i, t )
        {   var arr = XmlAspect.XPath_nl( xpath, t);
            for( i = 0; i<arr.length; i++ )
                $ret.push( arr[i] );
        });
        return $ret;
    }

    function loadXml(src)
    {
        if( !IS_STRING(src) && "$then" in src )
            return src;
        if( src instanceof $ )
            return src;    
        if( IS_DOM(src) )
            return $(src);
        if( 'string' == typeof src && src.charAt(0)=='<' )
            return $( XmlAspect.getXml(src) );

        var args = arguments
        ,   $ret = new $f( this, function( resolve, reject )
        {
            XmlAspect.getXml.apply( this, args )
                .then( function(xml)
                    {   //$.fn.push.call($ret,xml);resolve( $ret.result = $($ret) );
                        resolve( $ret.result = $(xml) );
                    }, function(ex)
                    {
                        reject(ex);
                    });
        });
        return $ret;
    }
//     $.Xml = function Xml( src )
//     {
//         return src instanceof $f ? src : $().loadXml(src);
//     };
    function IS_STRING  ( s ){  return 'string' === typeof s ;  }
    function IS_XMLSTR  ( s ){  return IS_STRING(s) && s.charAt(0)=='<';  }
    function IS_XML     ( n ){  return n && n.ownerDocument && n.ownerDocument.documentElement;  }
    function IS_DOM     ( s ){  return s.childNodes;            }
    function IS_FUNC    ( s ){  return 'function' == typeof s;  }
    function IS_PROMISE ( a ){  return a && IS_FUNC(a.then ) ;  }
    function IS_$PROMISE( a ){  return a && a instanceof $f;  }
    function IS_NUM     ( s ){  return 1*s == s;  }
    function eachProp( o, cb )
    {   var ret={};
        for( var k in o )
            ret[k]=cb.call(o,k);
        return ret;
    }

    /*** shimbhalla ***/
function Construct$( executor )
{
    var promise = new Promise(executor);

    this.promise = function(){ return promise; }
    this.orig_then = function(a,b){ return Promise.prototype.then.apply(promise, arguments); };
    this.destroy = function Construct$_destroy()
    {
        // releases resources in the promises chain behind and upfront of current one.
        //     1.  unlock/release/remove own resources. No async functionality shall be kept in queue.
        //     2.  invoke children[].destroy()
        //     3.  reject own promise with InterruptedException
        //     4.  detach from parent
        //     5.  invoke parent.destroy()
        
    }
    this.$then = function Construct$_then(a,b)
    {    if(!a)
        {   debugger;
            return promise;
        }
        var $r =  new $f( this, function $then_$f( resolve, reject )
        {
            promise.then( function $then_promise_then( a0 )
            {   try
                {   var r = a.apply( $r, buildResults( $r ) );
                    $r.result = r;
                    if( r && IS_FUNC(r.then) )
                        r.then( function $then_then_then(a0)
                                    {   resolve( $r.result = a0 ); }
                                , reject );
                    else
                        resolve(r);
                }catch(ex)
                    {   reject(ex); }
                return "TBD";                
            },function( ex )
            {   b && b(ex);
                reject(ex);
            });
        });
        return $r;
    };
    this.sleep = Construct$_sleep;
    
    return promise;
}
function Construct$_sleep( ms, cb )
{   // todo ms, cb type recognition; callback result into stack

    var zs = this
    ,   $r =  new $f( this, function $sleep_$f( resolve, reject )
    {
        zs.promise().then( function( a0 )
        {   setTimeout( function()
            {   try
                {   if( !cb)
                        return cc( ms === undefined ? $r : ms );
                    var r = cb.apply( $r, buildResults( $r ) );
                    if( !r )
                        cc(r);
                    else if( IS_FUNC( r.$then ) )
                        r.$then( cc, reject );
                    else if( IS_FUNC( r.then ) )
                        r.then( cc, reject );
                    else
                        cc( r )
                }catch(ex)
                    {   reject(ex); }
                function cc( ret )
                    {   resolve( $r.result = ret );   }
            }, ms||0 );
            return "TBD";
        }
        ,function( ex )
        {   b && b(ex);
            reject(ex);
        });
    });
    return $r;            
}

/*
    create SEGMENT ( method, branches[] )
        create action branch w/ Promise
        register branch in parent SEGMENT

        attach to parent branch Promise

    on parent branch Resolved
    execute method
        set promise.result, resolve /reject branch
    
    interrupt
        release resource if any
        reject branch Promise if any
    destroy
        ...
    fork 
        create action branch w/ Promise
        register branch in parent SEGMENT
        execute method
*/


function $f( jq, method, args )
{
    jq && jq.segments && jq.segments.push(this);

    this.parentQuery = jq;
    this.method = method;
    this.args = args;
    this.segments = [];
    this.branches = [];

    jQuery.fn.init.call( this, jq || [] );
    ignite.call( this, jq && jq.promise() );
}
$f.prototype = Object.create( jQuery.fn );
$f.prototype.constructor = $f;
$.extend(   $f.prototype,
        {   result : undefined
        ,   XPath : XPath_async
        ,   fork : fork
        ,   interrupt : interrupt
        ,   then : then
        ,   promise : promise
        });

$f.actions =
{   interrupt: 0
,   repeat: 0
};

function wrap( orig, method )
{
    wrapper.orig = function()
        {   return orig.apply( this, arguments );    };
    return wrapper;
    function wrapper()
        {   return new $f( this, method, arguments );  }
}
function fork( parent )
{   /** create new current segment tree clone with own promise chain starting from current segment.
        @return clone of current segment
    **/
    var f =  new $f( parent || this.parentQuery, this.method, this.args );
        this.segments.forEach( function (s)
        {
            s.fork(f);
        });
    return f;
}
function ignite( parentPromise )
{
    var zs = this
    ,   action = new $f.actions[ zs.method ]( zs )
    ,   p
    ,   ret ; 
    
    action.segment = this;
    action.promise = {parentPromise:parentPromise};// temp for cb args chain
    
    if( parentPromise && !( this.parentQuery instanceof $f ) )
        parentPromise.result = this.parentQuery;

    this.branches.push( action );
    p = new Promise( function( resolve, reject )
    {
        action.onComplete   = onComplete;
        action.OnFail       = OnFail;
        action.onAbort      = function onAbort( ex )
            {   reject( ex || new AbortInterruptedException(zs) ); };

        if( parentPromise )
            parentPromise.then( function()
            {   try{    return ret = action.run.apply(action, arguments); }
                catch( ex ){ OnFail( p.result = ex ); }
            }, OnFail );
        else
            setTimeout( function() // queue execution in order to complete init chain
            {   try{ ret = action.run() }
                catch( ex ){ OnFail( ex ); }
            }, 0 );
        // todo relocate exception handling here like in $Then_run
        var lastPromise;
        function onComplete(/* result of action */ a )
        {
            if( lastPromise === a )
                resolve( r(a) );
//             else if( a && IS_$PROMISE(a) )
//                 r( (lastPromise = a).$then( onComplete, OnFail ) );
            else if( a && IS_PROMISE(a) )
                r( (lastPromise = a).then ( onComplete, OnFail ) );
            else
                resolve( r(a) );

            function r(v)
            {   if( p )
                   p.result = v;
                return ret = v;
            }
        }
        function OnFail( ex )
        {   try{  action.interrupt( ex ) }
            catch( ex ){   action.onAbort(ex); }
        }
    });
    if( p.result !== ret )
        p.result = ret;
    action.promise = p;
    p.parentPromise = parentPromise;
p.debug_method =  zs.method;
    this.promise = function(){ return p };

    return zs;
}
function interrupt( ex )
{
    this.actions.forEach( function(a){ a.interrupt(ex || new AbortInterruptedException(zs)); }, this);
}
function then(a,b,c)
{
    return this.promise().then(a,b,c)
}
function promise( /* index of */ i )
{
    return at(this.branches, i).promise;
}
function forEach( arr, cb){ Array.prototype.forEach.call(arr,cb); }

function buildResults( $r )
{   var h = [];
    for( var p = $r.parentQuery; p ; p = p.parentQuery )
        h.push( p.result || p );
    return h;
}
function buildResults4promise( promise )
{
    var r = [];
    for( var p = promise.parentPromise; p ; p = p.parentPromise )
        r.push( 'result' in p ? p.result : p );
    return r;
}
function applyCallback( $o, cb, promise )
{
    return cb.apply( $o, buildResults4promise( promise ) );
}
function NOP(){}
function Action(){} // inspired by Thread api
$.extend( Action.prototype,
{   run:NOP
,   isAlive:NOP
,   interrupt: function(){ debugger; this.OnAbort(a0); }// should be explicitly overridden
//,   isInterrupted: function(){ return this.interruptedBy; }
//,   interruptedBy: undefined
});

inheritAction( Sleep, 'sleep' );
function Sleep( segment )
{   var a0 = segment.args[0], a1 = segment.args[1];
    this.ms = IS_NUM (a0)? a0 : a1 || 0;
    this.cb = IS_FUNC(a0)? a0 : IS_FUNC(a1)? a1 : 0;
}
$.extend( Sleep.prototype,
{   run : function Sleep_run()
    {   this.h = setTimeout( function()
        {   try{   this.onComplete( this.cb ? applyCallback( this.segment, this.cb, this.promise ) : this.ms );}
            catch( ex ){   OnFail(ex); }
        }.bind(this), this.ms );
    }
,   interrupt : function Sleep_interrupt(ex)
    {   if( this.h )
            this.h = clearTimeout(h) && 0;
        this.OnAbort(ex);
    }
,   isAlive : function Sleep_isAlive()
        {   return !!this.h }
});
$.sleep = function(){ return new $f( 0, 'sleep', arguments)};


inheritAction( $Then, '$then' );
function $Then( segment )
{   var cb = segment.args[0], eb = segment.args[1]
    ,   p;
    
    this.run = function $Then_run()
    {   p = this.promise;
return this.onComplete( cb ? applyCallback( segment, cb, p ) : cb );    
//         try{   this.onComplete( cb ? applyCallback( segment, cb, p ) : cb ); }
//         catch( ex )
//         {   p.result = ex;
//             try{   this.OnFail( eb ? applyCallback( segment, eb, {parentPromise:p} ) : eb ); }
//             catch( ex ){   this.OnFail(ex); }
//         }
    }.bind(this);
    this.interrupt = function $Then_interrupt(ex)
    {   if( p && p.result === ex ) // called on current segment execution failure
            this.onAbort(ex);
        else
            try
            {   p = this.promise;   
                return this.onComplete( eb ? applyCallback( segment, eb, this.promise ) : ex ); 
            }catch( ex )
            {   p.result = ex;
//                 this.onAbort( eb ? applyCallback( segment, eb, this.promise ) : ex);
                try{   this.OnFail( eb ? applyCallback( segment, eb, {parentPromise:p} ) : ex ); }
                catch( ex ){   this.OnFail(ex); }
            }
            
    }.bind(this);
    this.isAlive = function $Then_isAlive()
        {   return !!p };
}

inheritAction( Xml, 'xml' );
function Xml( segment )
{   var p;
    this.run = function Xml_run()
    {   p = this.promise;
        try
        {   var zs = this
            ,   src = segment.args[0];
            if( !IS_STRING(src) && "$then" in src )
                return this.onComplete( src );
            if( src instanceof $ )
                return this.onComplete( src );
            if( IS_DOM(src) )
                return this.onComplete( src ); // tbd $(src)
            if( 'string' == typeof src && src.charAt(0)=='<' )
                return this.onComplete( XmlAspect.getXml(src) );
            var xp = XmlAspect.getXml.apply( this, segment.args ).then( function( xml )
            {   segment.push.orig.call(segment,xml);
                return xml;
            });
            return this.onComplete( xp );
            //.then( function(xml)
            //    {   zs.onComplete( xml );
            //    }, function(ex)
            //    {   zs.OnFail(ex);
            //    });
        }catch( ex )
            {   this.OnFail(ex); }
    }.bind(this);
    this.interrupt = function Xml_interrupt(ex)
    {   // todo break XHRs
        this.onAbort(ex);
    }.bind(this);
    this.isAlive = function Xml_isAlive()
        {   return !!p };
}

inheritAction( jQAttr, 'jQAttr' );
function jQAttr( segment )
{   var p;
    this.run = function jQAttr_run()
    {   p = this.promise;
        try
        {   return this.onComplete( $.fn[segment.method].apply( $(segment), segment.args ) );
        }catch( ex )
            {   this.OnFail(ex); }
    }.bind(this);
    this.interrupt = function Xml_interrupt(ex)
    {   this.onAbort(ex);
    }.bind(this);
}
inheritAction( $On, '$on' );
function $On( segment )
{   var zs = this
    ,   evName = segment.args[0];
    this.run = function $On_run()
    {   try
        {   var pr = new Promise( function( resolve )
            {   // todo reject or check whether error flow is in place
                // todo full signature ..on/.off( events [, selector ] [, handler ] )
                var $s = $( segment ).on( evName, cb );
                function cb(ev)
                {   resolve(ev);
                    $s.off( evName, cb );
                    segment.fork();
                }
            });
            return zs.onComplete( pr );
        }catch( ex )
            {   zs.OnFail(ex); }
    };
    this.interrupt = function on_interrupt(ex)
    {   zs.off( evName );
    };
}


inheritAction( XPathAction, 'xPath' );
function XPathAction( segment )
{   var p;
    this.run = function XPathAction_run()
    {   p = this.promise;
        try
        {   var args = concatArr( segment.args, buildResults4promise( p ) );
            return this.onComplete( XPath.apply( $(segment), args ) );
        }catch( ex )
            {   this.OnFail(ex); }
    }.bind(this);
    this.interrupt = function Xml_interrupt(ex)
    {   this.onAbort(ex);
    }.bind(this);
}
inheritAction( XmlTransform, 'xmlTransform' );
function XmlTransform( segment )
{
    this.run = function XmlTransform_run()
    {   // $.xmlTransform( xml, xsl[, toEl] )
        // $(toEl).xmlTransform( xml, xsl )
        // $(fromXmlEl).xmlTransform( xsl[, toEl] )
        var params = parseXslArgs(segment, segment.args );

        var xml = params[0]
        ,   xsl = params[1]
        ,   to  = params[2];
        try
        {   var xp = IS_XML(xml) ? xml : ( xml.promise ? xml.promise() : XmlAspect.getXml(xml) )
            ,   sp = IS_XML(xsl) ? xsl : ( xsl.promise ? xsl.promise() : XmlAspect.getXml(xsl) )
            ,   rp = Promise.all([ xp,sp ]).then( function XmlTransform_xmlReady(arr)
                    {   return xmlTransform_sync( arr[0], arr[1], to );  });
rp.debug_all=[xp,sp];
            return this.onComplete( rp );
        }catch( ex )
            {   this.OnFail(ex); }
    }.bind(this);
    this.interrupt = function XmlTransform_interrupt(ex)
    {   this.onAbort(ex);
    }.bind(this);
}

function parseXslArgs( zs, args )
{
    var xml = args[0]
    ,   xsl = args[1]
    ,   to  = args[2];

    if( args.length == 2 && xsl instanceof $.fn.init ) // $.Xml(...).xmlTransform( xslUrl, $('.mywidget') )
    {   to = xsl;
        xsl = xml;
        xml = zs.parentQuery;
    }
    if( !to )
        to = zs instanceof $.fn.init ? zs : $();
    return [xml,xsl,to];
}

function xmlTransform( xml, xsl, to )
{
    var params = parseXslArgs(this, arguments);

    var xmlUrl, xslUrl;
    if( IS_STRING(xml) )
        if( IS_XMLSTR(xml) )
            xml = loadXml(xml);
        else
            xmlUrl = xml;
    if( IS_STRING(xsl) )
        if( IS_XMLSTR(xsl) )
            xsl = loadXml(xsl);
        else
            xslUrl = xsl;

    if( xmlUrl || xslUrl )
        return new $f( this, 'xmlTransform', params );
    return xmlTransform_sync.apply( this, params );
}
function xmlTransform_sync( xml, xsl, to )
{
    //if( !IS_STRING( to ) && to && "documentElement" in to )
    //    to = to.documentElement;
    var el = XmlAspect.transform( xml, xsl )
    ,   tDoc = ( to && to.ownerDocument ) || document
    ,   isString = IS_STRING( el );
    if( to )
    {   var $to = to.each ? to : $(to);
        $to.each( function( i, t )
        {   if( isString )
                return t.innerHTML = el;
            XmlAspect.cleanElement(t);
            t.appendChild( tDoc.importNode( el.documentElement|| el, true ) );
        });
    }else
    {   var $to = $();
        $to.push( el.documentElement || el );
    }
    return $to;
}

function inherit( child, parent){ child.prototype = new parent(); return child; }
function inheritAction( child, k ){  return $f.actions[k] = inherit(child, Action); }
function at( arr, i ){ return arr[ i>=0 ? i : i<0 ?  arr.length-i :  arr.length-1 ]; }
function concatArr(/* arr, arr...*/)
{   var ret = []; 
    forEach( arguments, function (arr)
    {   forEach(arr, function(el) 
            {   ret.push(el)    })  
    });
    return ret;
}

function InterruptedException(){}
function AbortInterruptedException(){}
function DestroyInterruptedException(){}

$f.InterruptedException         = inherit( InterruptedException         , Error                 );
$f.AbortInterruptedException    = inherit( AbortInterruptedException    , InterruptedException  );
$f.DestroyInterruptedException  = inherit( DestroyInterruptedException  , InterruptedException  );
eachProp( $.fn, function (k)
{
    if  (   'function' !== typeof this[k]
        ||  'constructor' == k
        ||  "|each|".indexOf('|'+k+'|') >= 0
        //||  k in $f.actions
        )
        return;
    var proto = $f.prototype;
    proto[k] = wrap( proto[k], k );
    if( !$f.actions[k] )
        $f.actions[k]=jQAttr;
});

$.fn.sleep = $f.prototype.sleep;
$.$then = function(){ return new $f( 0, '$then', arguments)};
$.fn.$then = function(){ return new $f( this, '$then', arguments)};
$.fn.$on = function(){ return new $f( this, '$on', arguments)};
$.xPath = function(xPath, el){ return $().xPath(xPath,el); };
$.xmlTransform = xmlTransform;
$.fn.xml = function(x){ return x instanceof $f ? x : new $f( this, 'xml', arguments)};
$.Xml= function(x){ return x instanceof $f || x instanceof $.fn.init ? x : new $f( 0, 'xml', arguments)};
$.Xml.Constructor = $f;
return $f;
}));